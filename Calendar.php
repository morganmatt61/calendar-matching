
<?php
class Calendar { 
    private $startTime = [];
    private $endTime = [];
    private $calendar = [];
    public $freeTimes = [];
    public $freeTimeMatches = [];

    function __construct(string $startTime, string $endTime, array $calendar) {
        $this->startTime = strtotime($startTime);
        $this->endTime = strtotime($endTime);
        foreach($calendar as $calendarItem) {
            $this->calendar[] = [strtotime($calendarItem[0]), strtotime($calendarItem[1])];
        }
        usort($this->calendar,[$this, "timeSort"]);
    }


    private function timeSort($a,$b) {
        if ($a[0] == $b[0]) {
            if ($a[1] == $b[1]) {
                return 0;
            }
            else {
                return ($a[1] < $b[1]) ? -1 : 1;
            }
        }
        return ($a[0] < $b[0]) ? -1 : 1;
    }


    public function findAndSetFreeTimes() {
        for( $i = 0; ( $i <= count($this->calendar)); $i++ ) {
            $endOfLast = (isset($this->calendar[$i-1][1])  && ($this->calendar[$i-1][1] > $this->startTime)) ? $this->calendar[$i-1][1] : $this->startTime;
            $beginningOfCurrent = (isset($this->calendar[$i][0])  && ($this->calendar[$i][0] < $this->endTime)) ? $this->calendar[$i][0] : $this->endTime;
            if ( $beginningOfCurrent > $endOfLast ) {
                $this->freeTimes[] = [$endOfLast, $beginningOfCurrent];
            }
        }
    }


    public function findAndSetMatch(array $freeTime1, array $freeTime2) {
        if ( $timeOverlap = $this->findTimeOverlap($freeTime1, $freeTime2)) {
            $this->freeTimeMatches[] = $timeOverlap;
        }
    }


    private function findTimeOverlap(array $time1, array $time2) {
        list($time1Start, $time1End) = $time1;
        list($time2Start, $time2End) = $time2;
        if (($time2Start <= $time1Start) && ($time2End > $time1Start)) {
            return [$time1Start, $this->findEndTime($time1End, $time2End)];
        } 
        if (($time2Start > $time1Start) && ($time2Start < $time1End)) {
            return [$time2Start, $this->findEndTime($time1End, $time2End)];
        }
        return false;
    }


    private function findEndTime(string $time1End, string $time2End) : string {
        return ($time1End < $time2End) ? $time1End : $time2End;
    }


    public function isDurationSufficient(array $freeTimeMatch, int $timePeriod) : bool {
        if (($freeTimeMatch[1] - $freeTimeMatch[0])/60 >= $timePeriod ) {
            return true;
        }
        return false;
    }
    

    public function getFormattedTime(array $time) : string {
        return gmdate("H:i", $time[0]) . " - " . gmdate("H:i", $time[1]);
    }
}
?>
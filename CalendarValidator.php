<?php
    class CalendarValidator { 
        public $failures = [];
        private $values;

        function __construct(stdClass $values) {
            $this->values = $values;
        }


        public function validateCalendars() : array {
            $this->allDataExists($this->values);
            $this->validateCalendar($this->values->calendar1);
            $this->validateCalendar($this->values->calendar2);  
            $this->validateTimeRange($this->values->dailyBounds1);
            $this->validateTimeRange($this->values->dailyBounds2);
            $this->validateMeetingDuration($this->values->meetingDuration);
            return $this->failures;
        }


        private function allDataExists() {
            if (!(
                isset($this->values->calendar1) &&
                isset($this->values->calendar2) &&
                isset($this->values->dailyBounds1) &&
                isset($this->values->dailyBounds2) &&
                isset($this->values->meetingDuration)
            )) {
                $this->failures[] = "calendar1, calendar2, dailyBounds1, dailyBounds2 and meetingDuration must be set."; 
            }
        }


        private function validateCalendar($calendar) {
            if (!is_array($calendar)) {
                 $this->failures[] = "Calendar must be an array.";
                 return;
            }
            foreach($calendar as $calendarItem) {
                $this->validateTimeRange($calendarItem);
            }
        }


        private function validateTimeRange($timeRange) {
            if (!( is_array($timeRange) && count($timeRange) == 2)) {
                $this->failures[] = "All time sets must be an array with a count of 2 times.";
                return;
            }
            foreach($timeRange as $time) {
                if (!$this->validTimeFormat($time)){
                    $this->failures[] = "Daily bounds time must be in 'hh:mm' format.";
                }
            }
            if ($timeRange[0] >= $timeRange[1]){
                $this->failures[] = "Start Times must be before End Times.";
            }
        }
        
        
        private function validTimeFormat($time){
            $dateObj = DateTime::createFromFormat("H:i", $time);
            return $dateObj && $dateObj->format("H:i") == $time;
        }




        private function validateMeetingDuration(string $meetingDuration) {
            if (($meetingDuration < 0) || ($meetingDuration > 1440)) {
                $this->failures[] = "Meeting duration must be between 0 and 1440";
            }
        }
    }
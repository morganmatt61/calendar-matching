<?php

require_once('Calendar.php');
require_once('CalendarValidator.php');


$values = json_decode( file_get_contents("calendar.json"));

$calendarValidator = new calendarValidator($values);
$calendarValidator->validateCalendars();
if (!empty(($failures = $calendarValidator->failures))) {
    echo "Data is in invalid format: " . PHP_EOL;
    foreach($failures as $failure) {
        echo $failure . PHP_EOL;
    }
    echo "Please fix format and run again." . PHP_EOL;
    die();
}

$calendar1 = new Calendar($values->dailyBounds1[0], $values->dailyBounds1[1], $values->calendar1);
$calendar2 = new Calendar($values->dailyBounds2[0], $values->dailyBounds2[1], $values->calendar2);

$calendar1->findAndSetFreeTimes();
$calendar2->findAndSetFreeTimes();

foreach($calendar1->freeTimes as $freeTime1) {
    foreach($calendar2->freeTimes as $freeTime2) {
        $calendar1->findAndSetMatch($freeTime1, $freeTime2);
    }
}

echo "Sufficient Matching Times: " . PHP_EOL;
foreach($calendar1->freeTimeMatches as $freeTimeMatch) {
    if ($calendar1->isDurationSufficient($freeTimeMatch, $values->meetingDuration)){
        echo $calendar1->getFormattedTime($freeTimeMatch) . PHP_EOL;
    }
}

?>
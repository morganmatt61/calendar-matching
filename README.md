# Calendar Matching

## Task
Imagine that you want to schedule a meeting of a specific duration with one of your co-workers. You have access to your calendar and your co-workers calendar (both of which contain the entirety of your respective meetings for the day in the form of `[startTime, endTime]`), as well as both of your daily bounds (i.e., the earliest and the latest times at which you're available for meetings every day, in the form of `[earliestTime, latestTime]`).

Write a function that takes in your calendar, your daily bounds, your co-worker's calendar, your co-worker's daily bounds, and the duration of the meeting that you want to schedule, and that returns a list of all the time blocks (in the form of [`startTime, endTime`]) during which you could schedule a meeting.

Note that times will be given and should be returned in military time. For example: `1:12`, `7:02`, and `17:38`.

_**Sample Input**_

```
calendar1 = [['9:00, '10:30'],['12:00, '13:00'], ['16:00, '18:00']]
dailyBounds1 = [['9:00, '20:30']
calendar2 = [['10:00, '11:30'],['12:30, '14:30'], ['14:30, '15:00'], ['16:00, '17:00']]
dailyBounds2 = [['10:00, '18:30']
meetingDuration = 30
```

_**Sample Output**_

`[['11:30, '12:00'],['15:00, '16:00'], ['18:00, '18:30']]`

## System Requirements

 1.  PHP 7

## Steps to run
 1. Simply run `php calendarMatching.php` from this directory.
 1. Please edit `calendar.json` to test out different scenarios. Format shown in file must be kept, including times being in `hh:mm` (`h:I`).
